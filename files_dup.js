var files_dup =
[
    [ "closedloop.py", "closedloop_8py.html", [
      [ "closedloop.Closedloop", "classclosedloop_1_1Closedloop.html", "classclosedloop_1_1Closedloop" ]
    ] ],
    [ "closedloop_6.py", "closedloop__6_8py.html", [
      [ "closedloop_6.Closedloop", "classclosedloop__6_1_1Closedloop.html", "classclosedloop__6_1_1Closedloop" ]
    ] ],
    [ "data_collection_task.py", "data__collection__task_8py.html", [
      [ "data_collection_task.Data_Collection_Task", "classdata__collection__task_1_1Data__Collection__Task.html", "classdata__collection__task_1_1Data__Collection__Task" ]
    ] ],
    [ "DRV8847.py", "DRV8847_8py.html", [
      [ "DRV8847.DRV8847", "classDRV8847_1_1DRV8847.html", "classDRV8847_1_1DRV8847" ],
      [ "DRV8847.Motor", "classDRV8847_1_1Motor.html", "classDRV8847_1_1Motor" ]
    ] ],
    [ "DRV8847_6.py", "DRV8847__6_8py.html", [
      [ "DRV8847_6.DRV8847", "classDRV8847__6_1_1DRV8847.html", "classDRV8847__6_1_1DRV8847" ],
      [ "DRV8847_6.Motor", "classDRV8847__6_1_1Motor.html", "classDRV8847__6_1_1Motor" ]
    ] ],
    [ "encoder.py", "encoder_8py.html", [
      [ "encoder.Encoder", "classencoder_1_1Encoder.html", "classencoder_1_1Encoder" ]
    ] ],
    [ "HW_0x01.py", "HW__0x01_8py.html", "HW__0x01_8py" ],
    [ "imu_driver.py", "imu__driver_8py.html", [
      [ "imu_driver.IMU_driver", "classimu__driver_1_1IMU__driver.html", "classimu__driver_1_1IMU__driver" ]
    ] ],
    [ "imu_driver_6.py", "imu__driver__6_8py.html", [
      [ "imu_driver_6.IMU_driver", "classimu__driver__6_1_1IMU__driver.html", "classimu__driver__6_1_1IMU__driver" ]
    ] ],
    [ "Lab0Fibonacci.py", "Lab0Fibonacci_8py.html", "Lab0Fibonacci_8py" ],
    [ "lab1.py", "lab1_8py.html", "lab1_8py" ],
    [ "main6.py", "main6_8py.html", "main6_8py" ],
    [ "main_lab2.py", "main__lab2_8py.html", "main__lab2_8py" ],
    [ "main_lab3.py", "main__lab3_8py.html", "main__lab3_8py" ],
    [ "main_lab4.py", "main__lab4_8py.html", "main__lab4_8py" ],
    [ "main_lab5.py", "main__lab5_8py.html", "main__lab5_8py" ],
    [ "mainpage.py", "mainpage_8py.html", null ],
    [ "motor_task.py", "motor__task_8py.html", [
      [ "motor_task.Task_motor", "classmotor__task_1_1Task__motor.html", "classmotor__task_1_1Task__motor" ]
    ] ],
    [ "motor_task_6.py", "motor__task__6_8py.html", [
      [ "motor_task_6.Task_motor", "classmotor__task__6_1_1Task__motor.html", "classmotor__task__6_1_1Task__motor" ]
    ] ],
    [ "panel_driver.py", "panel__driver_8py.html", [
      [ "panel_driver.Panel_driver", "classpanel__driver_1_1Panel__driver.html", "classpanel__driver_1_1Panel__driver" ]
    ] ],
    [ "share.py", "share_8py.html", [
      [ "share.Share", "classshare_1_1Share.html", "classshare_1_1Share" ],
      [ "share.Queue", "classshare_1_1Queue.html", "classshare_1_1Queue" ]
    ] ],
    [ "task_controller.py", "task__controller_8py.html", [
      [ "task_controller.Task_controller", "classtask__controller_1_1Task__controller.html", "classtask__controller_1_1Task__controller" ]
    ] ],
    [ "task_controller_6.py", "task__controller__6_8py.html", [
      [ "task_controller_6.Task_controller", "classtask__controller__6_1_1Task__controller.html", "classtask__controller__6_1_1Task__controller" ]
    ] ],
    [ "task_encoder_file.py", "task__encoder__file_8py.html", [
      [ "task_encoder_file.Task_encoder", "classtask__encoder__file_1_1Task__encoder.html", "classtask__encoder__file_1_1Task__encoder" ]
    ] ],
    [ "task_imu.py", "task__imu_8py.html", [
      [ "task_imu.Task_imu", "classtask__imu_1_1Task__imu.html", "classtask__imu_1_1Task__imu" ]
    ] ],
    [ "task_user.py", "task__user_8py.html", "task__user_8py" ],
    [ "task_user3.py", "task__user3_8py.html", "task__user3_8py" ],
    [ "task_user4.py", "task__user4_8py.html", "task__user4_8py" ],
    [ "task_user6.py", "task__user6_8py.html", "task__user6_8py" ],
    [ "touch_panel_task.py", "touch__panel__task_8py.html", [
      [ "touch_panel_task.Touch_Panel_Task", "classtouch__panel__task_1_1Touch__Panel__Task.html", "classtouch__panel__task_1_1Touch__Panel__Task" ]
    ] ]
];