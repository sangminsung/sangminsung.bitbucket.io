var classtask__controller__6_1_1Task__controller =
[
    [ "__init__", "classtask__controller__6_1_1Task__controller.html#ab8c3996c5c0868257e8c10bc8148eaf2", null ],
    [ "run", "classtask__controller__6_1_1Task__controller.html#af8c00c64547b9536f7882a0b0f7275b5", null ],
    [ "ball_position", "classtask__controller__6_1_1Task__controller.html#a97899a74c58a532e49c6c07a1a266013", null ],
    [ "ball_velocity", "classtask__controller__6_1_1Task__controller.html#af1bd24cbcf55736dfa9beccb43b03143", null ],
    [ "closedloop", "classtask__controller__6_1_1Task__controller.html#ae67fe6a77e2fd33e0b296c4038b4a5c6", null ],
    [ "duty", "classtask__controller__6_1_1Task__controller.html#ad104f7a7a389abbcd1eb9b25a4224653", null ],
    [ "K", "classtask__controller__6_1_1Task__controller.html#aede8d53d568c5e222e64f08c4d89edb3", null ],
    [ "panel_angle", "classtask__controller__6_1_1Task__controller.html#a102ed63bca183cc823b69c1edb2ef0f1", null ],
    [ "panel_velocity", "classtask__controller__6_1_1Task__controller.html#a425fe10c0ce4552801972f0bc5df1aa1", null ]
];