var annotated_dup =
[
    [ "closedloop", null, [
      [ "Closedloop", "classclosedloop_1_1Closedloop.html", "classclosedloop_1_1Closedloop" ]
    ] ],
    [ "closedloop_6", null, [
      [ "Closedloop", "classclosedloop__6_1_1Closedloop.html", "classclosedloop__6_1_1Closedloop" ]
    ] ],
    [ "data_collection_task", null, [
      [ "Data_Collection_Task", "classdata__collection__task_1_1Data__Collection__Task.html", "classdata__collection__task_1_1Data__Collection__Task" ]
    ] ],
    [ "DRV8847", null, [
      [ "DRV8847", "classDRV8847_1_1DRV8847.html", "classDRV8847_1_1DRV8847" ],
      [ "Motor", "classDRV8847_1_1Motor.html", "classDRV8847_1_1Motor" ]
    ] ],
    [ "DRV8847_6", null, [
      [ "DRV8847", "classDRV8847__6_1_1DRV8847.html", "classDRV8847__6_1_1DRV8847" ],
      [ "Motor", "classDRV8847__6_1_1Motor.html", "classDRV8847__6_1_1Motor" ]
    ] ],
    [ "encoder", null, [
      [ "Encoder", "classencoder_1_1Encoder.html", "classencoder_1_1Encoder" ]
    ] ],
    [ "imu_driver", null, [
      [ "IMU_driver", "classimu__driver_1_1IMU__driver.html", "classimu__driver_1_1IMU__driver" ]
    ] ],
    [ "imu_driver_6", null, [
      [ "IMU_driver", "classimu__driver__6_1_1IMU__driver.html", "classimu__driver__6_1_1IMU__driver" ]
    ] ],
    [ "motor_task", null, [
      [ "Task_motor", "classmotor__task_1_1Task__motor.html", "classmotor__task_1_1Task__motor" ]
    ] ],
    [ "motor_task_6", null, [
      [ "Task_motor", "classmotor__task__6_1_1Task__motor.html", "classmotor__task__6_1_1Task__motor" ]
    ] ],
    [ "panel_driver", null, [
      [ "Panel_driver", "classpanel__driver_1_1Panel__driver.html", "classpanel__driver_1_1Panel__driver" ]
    ] ],
    [ "share", null, [
      [ "Queue", "classshare_1_1Queue.html", "classshare_1_1Queue" ],
      [ "Share", "classshare_1_1Share.html", "classshare_1_1Share" ]
    ] ],
    [ "task_controller", null, [
      [ "Task_controller", "classtask__controller_1_1Task__controller.html", "classtask__controller_1_1Task__controller" ]
    ] ],
    [ "task_controller_6", null, [
      [ "Task_controller", "classtask__controller__6_1_1Task__controller.html", "classtask__controller__6_1_1Task__controller" ]
    ] ],
    [ "task_encoder_file", null, [
      [ "Task_encoder", "classtask__encoder__file_1_1Task__encoder.html", "classtask__encoder__file_1_1Task__encoder" ]
    ] ],
    [ "task_imu", null, [
      [ "Task_imu", "classtask__imu_1_1Task__imu.html", "classtask__imu_1_1Task__imu" ]
    ] ],
    [ "task_user", null, [
      [ "Task_user", "classtask__user_1_1Task__user.html", "classtask__user_1_1Task__user" ]
    ] ],
    [ "task_user3", null, [
      [ "Task_user", "classtask__user3_1_1Task__user.html", "classtask__user3_1_1Task__user" ]
    ] ],
    [ "task_user4", null, [
      [ "Task_user", "classtask__user4_1_1Task__user.html", "classtask__user4_1_1Task__user" ]
    ] ],
    [ "task_user6", null, [
      [ "Task_user", "classtask__user6_1_1Task__user.html", "classtask__user6_1_1Task__user" ]
    ] ],
    [ "touch_panel_task", null, [
      [ "Touch_Panel_Task", "classtouch__panel__task_1_1Touch__Panel__Task.html", "classtouch__panel__task_1_1Touch__Panel__Task" ]
    ] ]
];