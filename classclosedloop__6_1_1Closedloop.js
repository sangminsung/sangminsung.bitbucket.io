var classclosedloop__6_1_1Closedloop =
[
    [ "__init__", "classclosedloop__6_1_1Closedloop.html#a2677a9da43b5345927fc859b8f079c33", null ],
    [ "update", "classclosedloop__6_1_1Closedloop.html#a2d16fb92e4115ccfd7b9b46bb101cd09", null ],
    [ "ball_position", "classclosedloop__6_1_1Closedloop.html#a1489a2a02afdaaa86cb3817f6c818d7e", null ],
    [ "ball_velocity", "classclosedloop__6_1_1Closedloop.html#aab965f81cf7ec3c62285997b7abf9e8f", null ],
    [ "constant", "classclosedloop__6_1_1Closedloop.html#aaf6b8d458bdc578768b699a860813bc5", null ],
    [ "duty", "classclosedloop__6_1_1Closedloop.html#a507759360656048d9f27bfb82daa7ea6", null ],
    [ "K_num", "classclosedloop__6_1_1Closedloop.html#ac38326ecec10adad68ee7478f6644c1c", null ],
    [ "Kt", "classclosedloop__6_1_1Closedloop.html#a9962f85efc35cfab84b47d09b76f2ef7", null ],
    [ "newstate", "classclosedloop__6_1_1Closedloop.html#a151b5286d84db89b6ff3748d72c65619", null ],
    [ "R", "classclosedloop__6_1_1Closedloop.html#acf846edd54dca06e3764ec1aafa3c19e", null ],
    [ "statevector", "classclosedloop__6_1_1Closedloop.html#a9c4e88965be2b03f95bcd111a213b2a3", null ],
    [ "theta", "classclosedloop__6_1_1Closedloop.html#a8184262a13b0ae0ef4b12a444f22308e", null ],
    [ "Vdc", "classclosedloop__6_1_1Closedloop.html#ab98b3aa3dd0780b07aeac67a0d184cae", null ],
    [ "w", "classclosedloop__6_1_1Closedloop.html#a796d63511f83ebf2a748d076335c8806", null ]
];